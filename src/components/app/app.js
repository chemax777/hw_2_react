import React from "react";
import User from "../user/user"
import InputSearch from "../input-search/input-search";
import OptionList from "../option-list/option-list";
import OptionListFooter from "../option-list-footer/option-list-footer";

import iconsearch from "../../img/iconsearch.svg"

import "./app.css"



function App() {
    return (
        <>
            <nav className="navigation">
                <User name = "AnimatedFred" mail = "animated@demo.com"></User>
                <InputSearch src = {iconsearch} alt = "search-icon"></InputSearch>
                <OptionList></OptionList>
                <OptionListFooter></OptionListFooter>
            </nav>
        </>
    )
}

export default App