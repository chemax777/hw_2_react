import React from "react";

import "./input-search.css"

function InputSearch({src, alt}){
    return(
        <div className="navigation__input-search">
            <div className="input-search__icon"><img src={src} alt={alt} /></div>
            <input type="text" placeholder="Search..."/>
        </div>
    )
}

export default InputSearch