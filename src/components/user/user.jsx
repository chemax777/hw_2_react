import React from "react";

import "./user.css"


export function User({name, mail}) {
    return (
        <div className="navigation__user">
            <div className="user__icon">AF</div>
            <div className="user__info">
                <h1 className="user__name">{name}</h1>
                <h2 className="user__mail">{mail}</h2>
            </div>
        </div>
    )
}

export default User
