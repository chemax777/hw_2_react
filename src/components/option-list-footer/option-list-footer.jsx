import React from "react";
import Option from "../option-list/option/option";

import "./option-list-footer.css"

import logoutIcon from "../../img/logoutIcon.svg"
import darkmodeIcon from "../../img/darkmodeIcon.svg"
import OptionWithRadio from "../option-list/option-with-radio/option-with-radio";

function OptionListFooter(){
    return(
        <ul className="navigation__footer">
            <Option text = "Logout" src = {logoutIcon} alt = "logout icon"></Option>
            <OptionWithRadio text = "Darkmode" src = {darkmodeIcon} alt = "logout icon"></OptionWithRadio>
        </ul>
    )
}

export default OptionListFooter
