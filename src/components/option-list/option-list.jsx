import React from "react";
import Option from "./option/option";

import "./option-list.css"

import dashboardIcon from "../../img/dashboardIcon.svg"
import revenueIcon from "../../img/revenueIcon.svg"
import notificationsIcon from "../../img/notificationsIcon.svg"
import analyticsIcon from "../../img/analyticsIcon.svg"
import inventoryIcon from "../../img/inventoryIcon.svg"


function OptionList (){
    return(
        <ul className="navigation__option-list">
            <Option text = "Dashboard" src = {dashboardIcon} alt = "dashboard icon"></Option>
            <Option text = "Revenue" src = {revenueIcon} alt = "revenue icon"></Option>
            <Option text = "Notifications" src = {notificationsIcon} alt = "notification icon"></Option>
            <Option text = "Analytics" src = {analyticsIcon} alt = "notification icon"></Option>
            <Option text = "Inventory" src = {inventoryIcon} alt = "notification icon"></Option>
        </ul>
    )
}

export default OptionList