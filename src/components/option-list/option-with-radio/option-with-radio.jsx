import React from "react";

function OptionWithRadio ({text, src, alt}){
    return (
        <li className="option-list__item">
            <div className="option-list__item-icon">
                <img src={src} alt={alt} />
            </div>
            <a href="#">{text}</a>
        <label id="switch" class="switch">
            <input type="checkbox" id="slider"/>
            <span class="slider round"></span>
        </label>
        </li>
    )
}

export default OptionWithRadio