import React from "react";

function Option ({text, src, alt}){
    return (
        <li className="option-list__item">
            <div className="option-list__item-icon">
                <img src={src} alt={alt} />
            </div>
            <a href="#">{text}</a>
        </li>
    )
}

export default Option